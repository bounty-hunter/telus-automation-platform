package com.telus.automation.common.util;

import static com.telus.automation.common.util.Constants.*;
import static java.util.Objects.nonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/** This class is responsible for loading configuration from properties files.
 * @author jaikant
 *
 */
public class Config {

	// to make this singleton
		private Config() {

		}

		private static final Logger LOGGER = LoggerFactory.getLogger(Config.class);
		private static final String BASE_LOCATION = System.getProperty(USER_DIR) + File.separator + SRC + File.separator + MAIN + File.separator + RESOURCES
													+ File.separator + CONFIG + File.separator;
		private static final Properties PROPERTIES = new Properties();

		static {
			loadPropertiesFile(BASE_LOCATION + "telus.properties");
			loadPropertiesFile(BASE_LOCATION + "OR.properties");
		}	
		
		
		/**
		 * It will load properties file
		 * 
		 * @param filePath
		 */
		private static void loadPropertiesFile(String filePath) {
			LOGGER.info("################  Loading properties file : {} ", filePath);
			try {
				InputStream input = new FileInputStream(filePath);
				PROPERTIES.load(input);
				if (nonNull(input)) {
					input.close();
				}
			} catch (IOException e) {
				LOGGER.info("!!!! Exception while loading properties file : {} ", e.getMessage());
			}
		}
		
		/**
		 * It will give the property value present in properties file.
		 * 
		 * @param propertyName
		 * @return property value
		 */
		public static String getProperty(String propertyName) {
			
			return PROPERTIES.getProperty(propertyName);
		}
	
}
