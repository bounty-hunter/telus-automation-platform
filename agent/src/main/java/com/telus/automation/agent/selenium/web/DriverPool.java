package com.telus.automation.agent.selenium.web;


import static com.telus.automation.agent.selenium.web.Constants.*;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;

/**
 * It contains pool of web drivers for desktop web applications.
 *
 * @author jaikant
 */
public class DriverPool {
	static WebDriver driver = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(DriverPool.class);
    private static final String DRIVER_BASE_LOCATION = System.getProperty(USER_DIR) + File.separator + SRC + File.separator + MAIN + File.separator + RESOURCES + File.separator +  DRIVERS + File.separator;
   
    /** It returns WebDriver for the browser given
     * @param browser
     * @param nodeURL   URL where we want to initiate execution
     * @return  WebDriver
     */
    public static WebDriver getDriver(String browser, String nodeURL) {
       LOGGER.info("###################   Getting WebDriver of : {}", browser);
       
       try {
           if (!nodeURL.isEmpty()) {
               driver = getRemoteDriver(browser, nodeURL);
           } else {
               driver = getWebDriver(browser);
           }
       } catch(Exception e) {
           LOGGER.info("##########  EXCEPTION OCCURRED ##########: {}", e.getMessage());
       }
       driver.manage().window().maximize();
       
        return driver;
    }
    
    /**
     * @param browser   browser name
     * @param nodeURL   node URL where want to run execution
     * @return  RemoteWebDriver corresponding to the given browser value
     * @throws MalformedURLException
     */
    public static WebDriver getRemoteDriver(String browser, String nodeURL) throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        switch (browser.toLowerCase()) {
            case CHROME:
                caps = DesiredCapabilities.chrome();
                break;
            case FIREFOX:
                caps = DesiredCapabilities.firefox();
                break;
            case INTERNET_EXPLORER:
                caps = DesiredCapabilities.internetExplorer();
                break;
            default:
                caps = DesiredCapabilities.chrome();
                break;
        }
        caps.setPlatform(Platform.WINDOWS);

        return new RemoteWebDriver(new URL(nodeURL), caps);
    }

    /**
     * @param browser browser name
     * @return  WebDriver corresponding to the given browser value
     * @throws MalformedURLException
     */
    public static WebDriver getWebDriver(String browser) throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        WebDriver driver = null;
        switch (browser.toLowerCase()) {
            case CHROME:
                driver = getChromeDriver();
                break;
            case FIREFOX:
                driver = getFirefoxGeckoDriver();
                break;
            case INTERNET_EXPLORER:
                driver = getIEDriver();
                break;
            default:
                driver = getChromeDriver();
                break;
        }
        caps.setPlatform(Platform.WINDOWS);

        return driver;
    }

    /**
     * @return  instance of firefox gecko driver
     */
    public static WebDriver getFirefoxGeckoDriver() {
        System.setProperty("webdriver.gecko.driver", DRIVER_BASE_LOCATION + "geckodriver19.1.exe");
       
        return new FirefoxDriver();
    }

    /**
     * @return  instance of chrome driver
     */
    public static WebDriver getChromeDriver() {
        System.setProperty("webdriver.chrome.driver", DRIVER_BASE_LOCATION + "chromedriver.exe");
        
        return new ChromeDriver();
    }
    

    /** It gives instance of IEDriver
     * @return  instance of IEDriver
     */
    public static WebDriver getIEDriver() {
        System.setProperty("webdriver.ie.driver", DRIVER_BASE_LOCATION + "IEDriverServer.exe");
       
        return new InternetExplorerDriver();
    }

    public static ITestContext setContext(ITestContext iTestContext, WebDriver driver) {
    	iTestContext.setAttribute("driver", driver);
    	
    	return iTestContext;
    	
    }

}
