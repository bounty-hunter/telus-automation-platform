package com.telus.automation.agent.selenium.web;

import java.io.File;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.converters.ExtentHtmlLogConverter;
import com.telus.automation.common.util.Config;
import com.telus.automation.reporter.listener.ExtentManager;
import com.telus.automation.reporter.listener.CustomExtentReporter;


public class ElementUtils extends ExtentManager{
	
	public static WebDriver driver = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(ElementUtils.class);
	//protected static ExtentReports extent;
	
	
	public static void setDriver(WebDriver driver) {
		ElementUtils.driver = driver;
	}
	
	/**
	 * It will navigate to given url.
	 * 
	 * @param url
	 */
	public static void navigateToUrl(String url) throws Exception {
		try {
			//extent = ExtentManager.getInstance();
			CustomExtentReporter.test.log(Status.INFO, MarkupHelper.createLabel("Navigating to URL : "+url, ExtentColor.LIME));
			//extent = ExtentManager.getInstance();
			LOGGER.info("Navigating to URL--: {}", url);
			//test = extent.createTest("navigateToUrl "+url);
			driver.get(url);
			//test.log(Status.PASS, "Navigated to url");
			//extent.flush();
		} catch (Exception e) {
			LOGGER.error("Exception occurred while navigating:{}", e.getMessage());
			CustomExtentReporter.test.log(Status.ERROR, MarkupHelper.createLabel(e.getMessage(), ExtentColor.RED));
			//test.log(Status.FAIL, "Invalid URL Specified");
			throw new Exception();
		}
	}
	
	/**
	 * It will get element.
	 * 
	 * @param locator
	 * @throws Exception 
	 */
	public static WebElement getElement(String locator) throws Exception{
	    WebElement element = null;
		try {
			if (locator.endsWith("_xpath")) {
				LOGGER.info("Getting element for:-- {}", locator);
				element = driver.findElement(By.xpath(Config.getProperty(locator)));
				CustomExtentReporter.test.log(Status.INFO, MarkupHelper.createLabel("Found locator by xpath: "+locator , ExtentColor.LIME));
			} else if (locator.endsWith("_id")) {
				LOGGER.info("Getting element for:-- {}", locator);
				element = driver.findElement(By.id(Config.getProperty(locator)));
				CustomExtentReporter.test.log(Status.INFO, MarkupHelper.createLabel("Found locator by id: "+locator , ExtentColor.LIME));
			} else if (locator.endsWith("_name")) {
				LOGGER.info("Getting element for:-- {}", locator);
				element = driver.findElement(By.name(Config.getProperty(locator)));
				CustomExtentReporter.test.log(Status.INFO, MarkupHelper.createLabel("Found locator by name: "+locator , ExtentColor.LIME));
			} else if (locator.endsWith("_css")) {
				LOGGER.info("Getting element for:-- {}", locator);
				element = driver.findElement(By.cssSelector(Config.getProperty(locator)));
				CustomExtentReporter.test.log(Status.INFO, MarkupHelper.createLabel("Found locator by css: "+locator , ExtentColor.LIME));
			} else if (locator.endsWith("_linkText")) {
				LOGGER.info("Getting element for:-- {}", locator);
				element = driver.findElement(By.linkText(Config.getProperty(locator)));
				CustomExtentReporter.test.log(Status.INFO, MarkupHelper.createLabel("Found locator by linktext: "+locator , ExtentColor.LIME));
			}
		} catch (Exception e) {
			LOGGER.error("Exception occurred while clicking:{}", e.getMessage());
			CustomExtentReporter.test.log(Status.ERROR, MarkupHelper.createLabel("Locator not found : "+locator+" Exception Found : "+e.getMessage() , ExtentColor.RED));
			throw new Exception();
		}
		return element;
	}
	
	/**
	 * It will click on a given locator.
	 * 
	 * @param locator
	 */
	public static void click(String locator) throws Exception {
	
		try {
			LOGGER.info("Clicking on:-- {}", locator);
			getElement(locator).click();
		} catch (Exception e) {
			LOGGER.error("Exception occurred while clicking:{}", e.getMessage());
			CustomExtentReporter.test.log(Status.ERROR, MarkupHelper.createLabel("Unable to locate the element to be clicked: "+locator , ExtentColor.RED));
			throw new Exception();
		}
	}

	/**
	 * It will clear on a given locator.
	 * 
	 * @param locator
	 */
	public static void clear(String locator) throws Exception {

		try {
			LOGGER.info("Clicking on:-- {}", locator);
			getElement(locator).clear();
			CustomExtentReporter.test.log(Status.INFO, MarkupHelper.createLabel("Found locator by id: "+locator , ExtentColor.LIME));
		} catch (Exception e) {
			LOGGER.error("Exception occurred while clearing:{}", e.getMessage());
			throw new Exception();
		}
	}
	
	/**
	 * It will send data on a given input field.
	 * 
	 * @param locator
	 *            and data
	 */
	public static void input(String locator, String data) throws Exception {

		try {
		    LOGGER.info("Entering data on:-- {}{}", locator, data);
			getElement(locator).sendKeys(data);
			CustomExtentReporter.test.log(Status.INFO, MarkupHelper.createLabel("Sending text to field: "+locator , ExtentColor.LIME));
		} catch (Exception e) {
			LOGGER.error("Exception occurred while entering data :{}", e.getMessage());
			CustomExtentReporter.test.log(Status.ERROR, MarkupHelper.createLabel("Unable to send text : "+locator+" "+e.getMessage() , ExtentColor.RED));
			throw new Exception();
		}
	}

	/**
	 * It will return text from a given locator.
	 * 
	 * @param locator
	 */
	public static String getText(String locator) throws Exception {
		String text = null;
		try {
			LOGGER.info("Getting text:-- {}",locator);
			text= getElement(locator).getText();
		} catch (Exception e) {
			LOGGER.error("Exception occurred while returning data :{}", e.getMessage());
			throw new Exception();
		}
		return text;
	}

	/**
	 * It will select value from drop down for a given locator.
	 * 
	 * @param locator
	 *            and value
	 */
	public static void selectDropDownByVisibleText(String locator, String value) throws Exception {
		
		try {
			LOGGER.info("Selecting value from drop down:-- {}{}",locator,value);
			new Select(getElement(locator)).selectByVisibleText(value);
		}catch (Exception e) {
			LOGGER.error("Exception occurred while selecting value :{}", e.getMessage());
		}
	}
	
	/**
	 * It will take screenshot.
	 * 
	 * @param filename
	 */
	public static void captureScreenshot(String filename) {
		try {
			LOGGER.info("Taking ScreenShot");
			Calendar cal = Calendar.getInstance();
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile,
					new File(System.getProperty("user.dir") + "\\Screenshots\\" + filename + ".jpg"));
			LOGGER.info("ScreenShot is captured");
		} catch (Exception e) {
			LOGGER.error("Exception occurred while taking screenshot :{}", e.getMessage());
		}
	}
	
	/**
	 * It will close browser.
	 * 
	 */
	public static void closeBrowser() {
		LOGGER.info("Closing browser now");
		driver.quit();
		CustomExtentReporter.test.log(Status.INFO, MarkupHelper.createLabel("Browser Closed successfully" , ExtentColor.LIME));
	}
	
	/**
	 * It will click on a given locator using java script function.
	 * 
	 * @param locator
	 */
	public static void javaScriptClick(String locator) throws Exception {
		
		try {
			LOGGER.info("Clicking on:-- {}", locator);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", getElement(locator));

		} catch (Exception e) {
			LOGGER.error("Exception occurred while clicking :{}", e.getMessage());
			throw new Exception();
		}
	}
	
	/**
	 * It will clear a given input field using java script function.
	 * 
	 * @param locator
	 */
	public static void javaScriptClear(String locator) throws Exception {

		try {
			LOGGER.info("Clearing on:-- {}", locator);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].value ='';", getElement(locator));
		} catch (Exception e) {
			LOGGER.error("Exception occurred while clearing:{}", e.getMessage());
			throw new Exception();
		}
	}
	
	public static void javaScriptInput(String locator, String data) throws Exception {

		try {
			LOGGER.info("Entering data on:-- {}{}", locator, data);
			JavascriptExecutor executConfig = (JavascriptExecutor) driver;
			String s1 = "arguments[0].value='";
			String s2 = data + "';";
			String s = s1 + s2;
			executConfig.executeScript(s, getElement(locator));
		} catch (Exception e) {
			LOGGER.error("Exception occurred while entering:{}", e.getMessage());
			throw new Exception();

		}
	}
	
	
}
