package com.telus.automation.agent.selenium.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

public class CustomAssertion {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElementUtils.class);
	
	public void assertEquals(String actual, String expected) {
		LOGGER.info("#############  Asserting values : actual {} expected {}", actual, expected);
		Assert.assertEquals(actual, expected);
	}
}
