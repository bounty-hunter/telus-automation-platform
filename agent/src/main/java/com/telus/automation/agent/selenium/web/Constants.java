package com.telus.automation.agent.selenium.web;

public class Constants {

	// src/main/resources folder names
		public static final String RESOURCES = "resources";
		public static final String DRIVERS = "drivers";
		public static final String EXTENSIONS = "extensions";
		public static final String CONFIG = "config";

		public static final String USER_DIR = "user.dir";
		public static final String SRC = "src";
		public static final String MAIN = "main";
		public static final String DOUBLE_DOT = "..";

		// file extentions
		public static final String PNG_EXTENSION = ".png";
		public static final String XML_EXTENSION = ".xml";

		public static final String DRIVER = "driver";

		// attribute constants
		public static final String INNER_TEXT = "innerText";
		public static final String TEXT_CONTENT = "textContent";
		public static final String VALUE = "value";
		public static final String TITLE = "title";

		// browser constants
		public static final String FIREFOX = "firefox";
		public static final String CHROME = "chrome";
		public static final String INTERNET_EXPLORER = "internetexplorer";
	
}
