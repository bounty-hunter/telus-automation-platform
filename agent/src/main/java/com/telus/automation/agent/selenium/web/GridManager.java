package com.telus.automation.agent.selenium.web;

import java.io.File;
import java.io.IOException;
import static com.telus.automation.agent.selenium.web.Constants.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* @author jaikant
* This class is responsible for all selenium grid and server related operations.
*
*/
public class GridManager {
       
       public static void main(String[] args) {
              GridManager.initiateGridServer();
       }

       private static final Logger LOGGER = LoggerFactory.getLogger(GridManager.class);

       /**
       *  It will initiate the selenium standalone server required for selenium grid 
        */
       public static void initiateGridServer() {

              String seleniumStanaloneJarPath = System.getProperty(USER_DIR) + File.separator + SRC + File.separator + MAIN
                                                              + File.separator + RESOURCES + File.separator + DRIVERS + File.separator
                                                              + "selenium-server-standalone.jar";
              
              String[] hubConfig = new String[]{"java", "-jar", seleniumStanaloneJarPath, "-role","hub"};
              ProcessBuilder builder = new ProcessBuilder(hubConfig);
              File hubLog = new File(System.getProperty(USER_DIR) + File.separator + "hubLogs");
              builder.redirectErrorStream(true);
              builder.redirectOutput(hubLog);
              Process p;
              try {
                     p = builder.start();
              } catch (IOException e) {
                     LOGGER.error("Exception while initiating the grid server : {}", e.getMessage());
              }
       }


}
