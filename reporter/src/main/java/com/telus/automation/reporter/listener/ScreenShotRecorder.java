package com.telus.automation.reporter.listener;

import java.io.File;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ScreenShotRecorder {

	private static final Logger LOGGER = LoggerFactory.getLogger(ScreenShotRecorder.class);

	
	/**
	 * It will take screenshot.
	 * 
	 * @param filename
	 */
	public static String captureScreenshot(WebDriver driver, String filename) {
		String screenShotPath = System.getProperty("user.dir") + "\\ScreenShots\\" + filename + ".jpg";
		try {
			LOGGER.info("Taking ScreenShot");
			Calendar cal = Calendar.getInstance();
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile,
					new File(screenShotPath));
			LOGGER.info("ScreenShot is captured");
		} catch (Exception e) {
			LOGGER.error("Exception occurred while taking screenshot :{}", e.getMessage());
		}
		
		return screenShotPath;
	}
	
	
}
