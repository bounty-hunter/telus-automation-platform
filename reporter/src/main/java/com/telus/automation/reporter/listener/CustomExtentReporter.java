package com.telus.automation.reporter.listener;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;


public class CustomExtentReporter implements ITestListener {
	
	ExtentReports extent = ExtentManager.getInstance();
	public static ExtentTest test;

	public void onTestStart(ITestResult result) {
		System.out.println("onTestStart called ...");
		test = extent.createTest(result.getMethod().getMethodName());
	}

	public void onTestSuccess(ITestResult result) {
		//test = extent.createTest("passTest");
		System.out.println("onTestSuccess called ...");
		test.log(Status.PASS, MarkupHelper.createLabel(result.getMethod().getMethodName() + "Test Case Status is passed", ExtentColor.GREEN));
	}

	public void onTestFailure(ITestResult result) {
		WebDriver driver = (WebDriver) result.getTestContext().getAttribute("driver");
		System.out.println("onTestFailure called ...");
		test.log(Status.FAIL, MarkupHelper.createLabel(result.getMethod().getMethodName() + " - Test Case Failed", ExtentColor.RED));
		String screenShotPath = ScreenShotRecorder.captureScreenshot(driver, result.getMethod().getMethodName());
		test.log(Status.FAIL, MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
		try {
			test.addScreenCaptureFromPath(screenShotPath);
		} catch (IOException e) {
			test.log(Status.FAIL, "!!!!!!!!! Exception occurred while attaching the screenshot to extent report");
		}
		test.log(Status.FAIL, MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
		
	}

	public void onTestSkipped(ITestResult result) {
		System.out.println("onTestSkipped called ...");
		test.log(Status.SKIP, MarkupHelper.createLabel(result.getMethod().getMethodName() + " - Test Case Skipped", ExtentColor.ORANGE));
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("onTestFailedButWithinSuccessPercentage called ...");

	}

	public void onStart(ITestContext context) {
		System.out.println("onStart called ...");
		System.out.println("test name ::: "+ context.getName());
		//test = extent.createTest(context.getName());
	}

	public void onFinish(ITestContext context) {
		System.out.println("onFinish called ...");
		extent.flush();
		extent.removeTest(test);
	}

}
